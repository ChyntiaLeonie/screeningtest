//
//  GuessCollectionViewCell.swift
//  Screening Test
//
//  Created by Chyntia Leonie Andreas on 2/11/17.
//  Copyright © 2017 Chyntia Leonie Andreas. All rights reserved.
//

import UIKit

class GuestCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var imageGuest: UIImageView!
    @IBOutlet weak var namaGuest: UILabel!
}
