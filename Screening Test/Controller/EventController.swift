//
//  EventController.swift
//  Screening Test
//
//  Created by Chyntia Leonie Andreas on 2/10/17.
//  Copyright © 2017 Chyntia Leonie Andreas. All rights reserved.
//


protocol EventControllerDelegate {
    func dataClicked(nameEvent : String)
}

import UIKit

class EventController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    
    var delegate: EventControllerDelegate?
    
    // These strings will be the data for the table view cells
    let allEvent : NSMutableArray = NSMutableArray()
    
    // Don't forget to enter this in IB also
    let cellReuseIdentifier = "cellEvent"
    
    @IBOutlet weak var tableView: UITableView!
    override func viewDidLoad(){
        super.viewDidLoad()
        
        
        var event : Event = Event()
        event.imageEvent = "1"
        event.nameEvent = "abc event"
        event.dateEvent = "14 Sept 2014"
        allEvent.addObject(event)
        
        event = Event()
        event.imageEvent = "2"
        event.nameEvent = "bcd event"
        event.dateEvent = "15 Sept 2014"
        allEvent.addObject(event)
        
        event = Event()
        event.imageEvent = "3"
        event.nameEvent = "cde event"
        event.dateEvent = "16 Sept 2014"
        allEvent.addObject(event)
        
        event = Event()
        event.imageEvent = "4"
        event.nameEvent = "def event"
        event.dateEvent = "17 Sept 2014"
        allEvent.addObject(event)
        
        tableView.delegate = self
        tableView.dataSource = self
    }
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        
    }
    
    // number of rows in table view
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.allEvent.count
    }
    
    // create a cell for each table view row
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell:EventTableViewCell = self.tableView.dequeueReusableCellWithIdentifier(cellReuseIdentifier) as! EventTableViewCell
        
        cell.imageEvent.image = UIImage(named : allEvent[indexPath.row].imageEvent)
        cell.nameEvent.text = allEvent[indexPath.row].nameEvent
        cell.dateEvent.text = allEvent[indexPath.row].dateEvent
        
        
        return cell;
    }
    
    
    // method to run when table view cell is tapped
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        if let del = delegate {
            print("masuk")
            del.dataClicked(allEvent[indexPath.row].nameEvent)
            self.dismissViewControllerAnimated(true, completion: {});
        }
        
    }
    
}