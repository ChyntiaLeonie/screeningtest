//
//  Event:GuessController.swift
//  Screening Test
//
//  Created by Chyntia Leonie Andreas on 2/10/17.
//  Copyright © 2017 Chyntia Leonie Andreas. All rights reserved.
//

import UIKit

class EventGuestController: UIViewController, EventControllerDelegate, GuestControllerDelegate {
    
    @IBOutlet weak var nama: UILabel!
    
    
    var sNama : String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        nama.text = sNama
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    @IBOutlet weak var btnEvent: UIButton!
    @IBOutlet weak var btnGuest: UIButton!
    
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        guard let segueId = segue.identifier else { return }
        
        switch segueId {
        case "segueEvent":
            let destVC = segue.destinationViewController as! EventController
            destVC.delegate = self
            break
        case "segueGuest":
            let destVC = segue.destinationViewController as! GuestController
            destVC.delegate = self
            break
        default:
            break
        }
    }
    
    //Child Delegate
    func dataClicked(nameEvent: String){
        btnEvent.setTitle(nameEvent, forState: .Normal)
    }
    func dataGuestClicked(nameGuest: String, isiToast: String){
        
        btnGuest.setTitle(nameGuest, forState: .Normal)
    }
    
    
}
