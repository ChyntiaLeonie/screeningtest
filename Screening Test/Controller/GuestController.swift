//
//  GuessController.swift
//  Screening Test
//
//  Created by Chyntia Leonie Andreas on 2/10/17.
//  Copyright © 2017 Chyntia Leonie Andreas. All rights reserved.
//

import UIKit


protocol GuestControllerDelegate {
    func dataGuestClicked(nameGuest : String, isiToast : String)
}

class GuestController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource {
    
    var delegate: GuestControllerDelegate?
    
    let allGuest:NSMutableArray = NSMutableArray()
    
    override func viewDidLoad(){
        super.viewDidLoad()
        
        
        var guest : Guest = Guest()
        guest.pictGuest = "b1"
        guest.idGuest = 1
        guest.nameGuest = "Andi"
        guest.birthdateGuest = "2014-01-01"
        allGuest.addObject(guest)
        
        guest = Guest()
        guest.pictGuest = "b2"
        guest.idGuest = 2
        guest.nameGuest = "Budi"
        guest.birthdateGuest = "2014-02-02"
        allGuest.addObject(guest)
        
        guest = Guest()
        guest.pictGuest = "b3"
        guest.idGuest = 3
        guest.nameGuest = "Charlie"
        guest.birthdateGuest = "2014-03-03"
        allGuest.addObject(guest)
        
        guest = Guest()
        guest.pictGuest = "b4"
        guest.idGuest = 4
        guest.nameGuest = "Dede"
        guest.birthdateGuest = "2014-06-06"
        allGuest.addObject(guest)
        
        guest = Guest()
        guest.pictGuest = "b5"
        guest.idGuest = 5
        guest.nameGuest = "Joko"
        guest.birthdateGuest = "2014-02-12"
        allGuest.addObject(guest)
    }
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        
    }
    
    //Collection View
    
    let reuseIdentifier = "cellGuest" // also enter this string as the cell identifier in the storyboard
    
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.allGuest.count
    }
    
    // make a cell for each cell index path
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        
        // get a reference to our storyboard cell
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier(reuseIdentifier, forIndexPath: indexPath) as! GuestCollectionViewCell
        
        // Use the outlet in our custom class to get a reference to the UILabel in the cell
        cell.imageGuest.image = UIImage(named : allGuest[indexPath.row].pictGuest)
        cell.namaGuest.text = allGuest[indexPath.row].nameGuest
        
        return cell
    }
    
    // MARK: - UICollectionViewDelegate protocol
    
    func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
        // handle tap events
        if let del = delegate {
            let birthdateGuestArr = allGuest[indexPath.row].birthdateGuest.characters.split{$0 == "-"}.map(String.init)
            var isiToast : String = "Feature Phone "
            if Int(birthdateGuestArr[2])! % 2 == 0 && Int(birthdateGuestArr[2])! % 3 == 0 {
                isiToast = "IoS"
            }
            else if Int(birthdateGuestArr[2])! % 2 == 0 {
                isiToast = "BlackBerry"
            }
            else if Int(birthdateGuestArr[2])! % 3 == 0 {
                isiToast = "Android"            }
            
            del.dataGuestClicked(allGuest[indexPath.row].nameGuest, isiToast: isiToast)
            
            let alertController = UIAlertController(title: "", message:
                isiToast, preferredStyle: UIAlertControllerStyle.Alert)
            alertController.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Default,handler: {action in
                
                self.dismissViewControllerAnimated(true, completion: {});
                }))
            self.presentViewController(alertController, animated: true, completion: nil)
            
            
        }
    }

    
}