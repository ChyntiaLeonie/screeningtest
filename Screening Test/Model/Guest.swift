//
//  Guest.swift
//  Screening Test
//
//  Created by Chyntia Leonie Andreas on 2/11/17.
//  Copyright © 2017 Chyntia Leonie Andreas. All rights reserved.
//

import UIKit

class Guest: NSObject{
    var pictGuest : String = String()
    var idGuest : Int = Int()
    var nameGuest : String = String()
    var birthdateGuest : String = String()
}
